<?php 

require_once 'Models/De.php';

class Generateur
{
    public static function getCarac()
    {
        $de = new De(6);

        $stats = [];

        for($i = 0; $i < 4; $i++)
        {
            $stats[] = $de->lancer();
        }

        arsort($stats);

        $result = 0;
        foreach($stats as $st)
        {
            $result += $st; 
        }

        return $result;
    }

    public static function getModificateur($value)
    {
        if($value < 5)
        {
            return -1;
        } elseif($value < 10)
        {
            return 0;
        } elseif($value < 15)
        {
            return 1;
        } else 
        {
            return 2;
        }
    }
}

?>