<?php 

require_once 'Models/Dragonnet.php';
require_once 'Models/Nain.php';

$hero = new Nain("Gloin");

while(!$hero->estMort())
{
    $monstre = new Dragonnet();

    echo "vous rencontrez un Dragonnet ! \n";

    $initiative = rand(0, 1);
    while(!$hero->estMort() && !$monstre->estMort())
    {
        if($initiative)
        {
            $hero->frappe($monstre);
            echo "le hero attaque! \n";
        } else 
        {
            $monstre->frappe($hero);
        }
        $initiative != $initiative;
    }

    if(!$hero->estMort())
    {
        echo "le hero a vaincu un dragonnet \n";
        $hero->loot($monstre);
        $hero->seReposer();
    }
    echo "\n";
}

echo "le hero est mort!\n";
echo "butin : " . $hero->or . " or " . $hero->cuir . " cuir \n";

?>