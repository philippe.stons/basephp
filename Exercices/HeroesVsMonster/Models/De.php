<?php 

class De
{
    private $max;
    private $min;

    public function __construct($max)
    {
        $this->max = $max; 
        $this->min = 1;   
    }

    public function lancer()
    {
        return rand($this->min, $this->max);
    }
}

?>