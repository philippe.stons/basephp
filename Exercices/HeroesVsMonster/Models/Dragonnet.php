<?php 

require_once 'Monstre.php';
require_once 'Interface/iCuir.php';
require_once 'Interface/iOr.php';

class Dragonnet extends Monstre implements iCuir, iOr
{
    private $cuir;
    private $or;

    public function __construct()
    {
        parent::__construct();
        $this->cuir = $this->d4->lancer();
        $this->or = $this->d6->lancer();
    }

    public function getCuir()
    {
        return $this->cuir;
    }

    public function getOr()
    {
        return $this->or;
    }
}

?>