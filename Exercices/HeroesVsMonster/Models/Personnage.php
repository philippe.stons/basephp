<?php 

require_once 'De.php';
require_once 'Tool/Generateur.php';

class Personnage
{
    protected $endurance;
    protected $force;
    protected $pv;
    protected $d4;
    protected $d6;

    public function __construct()
    {
        $this->endurance = Generateur::getCarac();
        $this->force = Generateur::getCarac();
        $this->pv = $this->pdvMax();
        $this->d4 = new De(4);
        $this->d6 = new De(6);
    }

    public function getEndurance()
    {
        return $this->endurance;
    }

    public function getForce()
    {
        return $this->force;
    }

    public function getPv()
    {
        return $this->pv;
    }

    public function setPv(int $Pv)
    {
        if($Pv <= $this->pdvMax())
            $this->pv = $Pv;
    }

    public function pdvMax()
    {
        return $this->endurance + Generateur::getModificateur($this->endurance);
    }

    public function estMort()
    {
        return $this->pv <= 0;
    }

    public function frappe(Personnage $p)
    {
        $degat = $this->d4->lancer() + Generateur::getModificateur($this->force);
        $p->pv -= $degat;
    }
}

?>