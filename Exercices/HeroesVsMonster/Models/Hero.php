<?php 

require_once 'Personnage.php';
require_once 'Interface/iOr.php';
require_once 'Interface/iCuir.php';

class Hero extends Personnage
{
    public $or;
    public $cuir;
    public $nom;

    public function __construct($nom)
    {
        parent::__construct();
        $this->nom = $nom;
        $this->or = 0;
        $this->cuir = 0;
    }

    public function getPieceOr()
    {
        return $this->or;
    }

    public function getCuir()
    {
        return $this->cuir;
    }

    public function loot(Personnage $monstre)
    {
        if($monstre instanceof iOr)
        {
            $this->or += $monstre->getOr();
        }

        if($monstre instanceof iCuir)
        {
            $this->cuir += $monstre->getCuir();
        }
    }

    public function seReposer()
    {
        $this->pv = $this->pdvMax();
    }
}

?>