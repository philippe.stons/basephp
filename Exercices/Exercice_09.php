<?php 

class Actor
{
    public $id;
    public $first_name;
    public $last_name;

    public function __construct($id, $first_name, $last_name)
    {
        $this->id = $id;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
    }

    public function __toString()
    {
        return "ID : $this->id First name : $this->first_name Last name : $this->last_name \n";
    }
}

class Connection
{
    private static $pdo;

    public function __construct()
    {
    }

    private static function connect()
    {
        if(self::$pdo == null)
        {
            self::$pdo = new PDO('mysql:host=127.0.0.1;dbname=movie_node_express;charset=utf8', // database info
                "root", // username
                ""); // password
        }
        return self::$pdo;
    }

    public function getActors()
    {
        $pdo = self::connect();
        $stmt = $pdo->prepare("SELECT * FROM actor");
        $stmt->execute();

        return $stmt;
    }

    public function getActorsByLastName($lastName)
    {
        $stmt = self::connect()->prepare("SELECT * FROM actor WHERE last_name=:last_name");
        $stmt->execute(["last_name"=>$lastName]);

        return $stmt;
    }
}

$input = "";
$conn = new Connection();

while($input != "quit")
{
    $input = readline("entrer un nom : ");
    $query = $conn->getActorsByLastName($input);
    $data = $query->fetchAll();
    echo "\n";
    $result = [];
    foreach($data as $row)
    {
        //$result[] = new Actor(/* params */);
        $result[] = new Actor($row["id"], $row["first_name"], $row["last_name"]);
        //echo "\t- " . $row["first_name"] . " " . $row["last_name"] . "\n";

        // Creer un tableau d'"actor" dans ce foreach.
    }

    //imprimer le tableau en utilisant la fonction __toString();
    foreach($result as $val)
    {
        echo $val;
    }

    echo "\n";
}



?>