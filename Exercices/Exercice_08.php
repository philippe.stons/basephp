<?php 

$pdo = new PDO('mysql:host=127.0.0.1;dbname=movie_node_express;charset=utf8', // database info
    "root", // username
    ""); // password

$stmt = $pdo->prepare("SELECT first_name, last_name FROM actor WHERE first_name=:first_name AND last_name=:last_name");
$stmt->execute(["first_name" => "Clara", "last_name" => "Schumann"]);

$data = $stmt->fetch();

var_dump($data);

$stmt = $pdo->prepare("SELECT * FROM actor WHERE last_name=:last_name");
$stmt->execute(["last_name" => "Bruckner"]);

$data = $stmt->fetch();

echo $data["first_name"] . " " . $data["last_name"];

?>