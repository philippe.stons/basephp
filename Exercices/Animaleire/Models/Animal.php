<?php 

abstract class Animal
{
    protected $nom;
    protected $poids;
    protected $taille;
    protected $sexe;
    protected $dateNaissance;
    protected $chanceDeces;
    protected $estVivant;
    protected $estAdopte;
    protected $dateArrive;

    public function __construct(string $nom, float $poids, float $taille, bool $sexe, 
        DateTime $dateNaissance, float $chanceDeces, bool $estVivant, bool $estAdopte, DateTime $dateArrive)
    {
        $this->nom = $nom;
        $this->poids = $poids;
        $this->taille = $taille;
        $this->sexe = $sexe;
        $this->dateNaissance = $dateNaissance;
        $this->chanceDeces = $chanceDeces;
        $this->estVivant = $estVivant;
        $this->estAdopte = $estAdopte;
        $this->dateArrive = $dateArrive;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($value)
    {
        $this->nom = $value;
    }

    public function getPoids()
    {
        return $this->poids;
    }

    public function setPoids($value)
    {
        $this->poids = $value;
    }

    public function getTaille()
    {
        return $this->taille;
    }

    public function setTaille($value)
    {
        $this->taille = $value;
    }

    public function getSexe()
    {
        return $this->sexe;
    }

    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    public function getChanceDeces()
    {
        return $this->chanceDeces;
    }

    public function getEstVivant()
    {
        return $this->estVivant;
    }

    public function setEstVivant($value)
    {
        $this->estVivant = $value;
    }

    public function getEstAdopte()
    {
        return $this->estAdopte;
    }

    public function setEstAdopte($value)
    {
        $this->estAdopte = $value;
    }

    public function getDateArrive()
    {
        return $this->dateArrive;
    }

    public function age()
    {
        $now = new DateTime();
        $interval = $now->diff($this->dateNaissance);
        return $interval->y;
    }

    abstract public function ageHumain();
    abstract public function crier();
}

?>