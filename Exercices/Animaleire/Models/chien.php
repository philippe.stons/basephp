<?php 

require_once 'Animal.php';

class Chien extends Animal
{
    private $couleurCollier;
    private $race;
    private $estDresse;

    public function __construct(string $nom, float $poids, float $taille, bool $sexe, 
        DateTime $dateNaissance, bool $estVivant, bool $estAdopte, DateTime $dateArrive,
        bool $couleurCollier, string $race, bool $estDresse)
    {
        parent::__construct($nom, $poids, $taille, $sexe, $dateNaissance, 0.5, $estVivant, $estAdopte, $dateArrive);
        $this->couleurCollier = $couleurCollier;
        $this->race = $race;
        $this->estDresse = $estDresse;
    }

    public function getCouleurCollier()
    {
        return $this->couleurCollier;
    }

    public function getRace()
    {
        return $this->race;
    }

    public function getEstDresse()
    {
        return $this->estDresse;
    }

    public function ageHumain()
    {
        return $this->age() * 7;
    }

    public function crier()
    {
        echo "Waouf\n";
    }

}

?>