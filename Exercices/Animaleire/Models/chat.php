<?php 

require_once 'Animal.php';

class Chat extends Animal
{
    private $poilLong;
    private $caractere;
    private $tailleGriffe;
    private $tailleGriffeMax;

    public function __construct(string $nom, float $poids, float $taille, bool $sexe, 
        DateTime $dateNaissance, bool $estVivant, bool $estAdopte, DateTime $dateArrive,
        bool $poilLong, string $caractere)
    {
        parent::__construct($nom, $poids, $taille, $sexe, $dateNaissance, 0.5, $estVivant, $estAdopte, $dateArrive);
        $this->poilLong = $poilLong;
        $this->caractere = $caractere;
        $this->tailleGriffe = 0;
        $this->tailleGriffeMax = 10;
    }

    public function getCaractere()
    {
        return $this->caractere;
    }

    public function getPoilLong()
    {
        return $this->poilLong;
    }

    public function getGriffeLongues()
    {
        return $this->tailleGriffe >= $this->tailleGriffeMax;
    }

    public function ageHumain()
    {
        return $this->age() * 5;
    }

    public function crier()
    {
        echo "Miaou\n";
    }

}

?>