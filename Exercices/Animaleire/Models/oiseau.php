<?php 

require_once 'Animal.php';

class Oiseau extends Animal
{
    // Attribut / Membre / 
    private $couleur;
    private $cageVoliere;

    public function __construct(string $nom, float $poids, float $taille, bool $sexe, 
        DateTime $dateNaissance, bool $estVivant, bool $estAdopte, DateTime $dateArrive,
        bool $couleur, string $cageVoliere)
    {
        parent::__construct($nom, $poids, $taille, $sexe, $dateNaissance, 3.0, $estVivant, $estAdopte, $dateArrive);
        $this->couleur = $couleur;
        $this->cageVoliere = $cageVoliere;
    }

    public function getCouleur()
    {
        return $this->couleur;
    }

    public function getcageVoliere()
    {
        return $this->cageVoliere;
    }

    public function ageHumain()
    {
        return $this->age();
    }

    public function crier()
    {
        echo "Piou piou piou\n";
    }

}

?>