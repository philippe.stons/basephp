<?php

require_once 'Personne.php';

class Compte
{
    private $numero;
    private $solde;
    private $ligneCredit;
    private $titulaire;

    public function __construct(string $numero, float $solde, float $ligneCredit, Personne $titulaire)
    {
        $this->numero = $numero;
        $this->solde = $solde;
        $this->ligneCredit = $ligneCredit;
        $this->titulaire = $titulaire;
    }

    public function getNumero()
    {
        return $this->numero;
    }

    public function setNumero($value)
    {
        if(is_string($value))
        {
            $this->numero = $value;
        }
    }

    public function getSolde()
    {
        return $this->solde;
    }

    public function getLigneCredit()
    {
        return $this->ligneCredit;
    }

    public function setLigneCredit($value)
    {
        if(is_float($value))
        {
            if($value < 0)
            {
                return;
            }
            $this->ligneCredit = $value;
        }
    }

    public function getTitulaire()
    {
        return $this->titulaire;
    }

    public function setTitulaire(Personne $value)
    {
        $this->titulaire = $value;
    }

    public function retrait($montant)
    {
        if(is_numeric($montant))
        {
            if($montant <= 0)
            {
                return;
            }

            if($this->solde - $montant < -$this->ligneCredit)
            {
                return;
            }

            $this->solde -= $montant;
        }
    }

    public function depot($montant)
    {
        if(is_numeric($montant))
        {
            if($montant <= 0)
            {
                return;
            }
    
            $this->solde += $montant;
        }
    }
}

?>