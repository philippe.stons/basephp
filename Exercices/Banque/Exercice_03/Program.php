<?php 

require_once 'Models/Courant.php';
require_once 'Models/Epargne.php';
require_once 'Models/Personne.php';
require_once 'Models/Banque.php';

$p = new Personne("John", "Doe", new DateTime("2000-01-01"));
$c = new Epargne("0000001", 0.0, $p);
$c2 = new Courant("0000002", 0.0, $p);
$b = new Banque("ING");

$b->AjouterCompte($c);
$b->AjouterCompte($c2);

$b["0000001"]->depot(500);
$b["0000001"]->retrait(300);

$b["0000002"]->depot(500);
$b["0000002"]->depot(500);

echo $b["0000001"]->getSolde() . "\n";
echo $b["0000002"]->getSolde() . "\n";

?>