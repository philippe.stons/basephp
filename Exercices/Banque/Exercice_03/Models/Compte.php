<?php

require_once 'Personne.php';

abstract class Compte
{
    private $numero;
    private $solde;
    private $titulaire;

    public function __construct(string $numero, float $solde, Personne $titulaire)
    {
        $this->numero = $numero;
        $this->solde = $solde;
        $this->titulaire = $titulaire;
    }

    public function getNumero()
    {
        return $this->numero;
    }

    public function setNumero($value)
    {
        if(is_string($value))
        {
            $this->numero = $value;
        }
    }

    public function getSolde()
    {
        return $this->solde;
    }

    public function getTitulaire()
    {
        return $this->titulaire;
    }

    public function setTitulaire(Personne $value)
    {
        $this->titulaire = $value;
    }

    public function retrait($montant)
    {
        $this->_retrait($montant, 0.0);
    }

    protected function _retrait($montant, $ligneCredit)
    {
        if(is_numeric($montant))
        {
            if($montant <= 0)
            {
                return;
            }

            if($this->solde - $montant < -$ligneCredit)
            {
                return;
            }

            $this->solde -= $montant;
        }
    }

    public function depot($montant)
    {
        if(is_numeric($montant))
        {
            if($montant <= 0)
            {
                return;
            }
    
            $this->solde += $montant;
        }
    }
}

?>