<?php 

require_once 'Compte.php';

class Courant extends Compte
{
    private $ligneDeCredit;

    public function __construct(string $numero, float $solde, Personne $titulaire)
    {
        parent::__construct($numero, $solde, $titulaire);
        $this->ligneCredit = 0.0;
    }

    public function getLigneCredit()
    {
        return $this->ligneCredit;
    }

    public function setLigneCredit($value)
    {
        if(is_float($value))
        {
            if($value < 0)
            {
                return;
            }
            $this->ligneCredit = $value;
        }
    }

    // Override de la fonction retrait.
    public function retrait($montant)
    {
        $this->_retrait($montant, $this->ligneDeCredit);
    }
}

?>