<?php 

require_once 'Compte.php';

class Epargne extends Compte
{
    private $dernierRetrait;

    public function __construct(string $numero, float $solde, Personne $titulaire)
    {
        parent::__construct($numero, $solde, $titulaire);
        $this->dernierRetrait = new DateTime();
    }

    public function getdernierRetrait()
    {
        return $this->dernierRetrait;
    }

    public function setdernierRetrait(DateTime $value)
    {
        $this->dernierRetrait = $value;
    }

    // Override de la fonction retrait.
    public function retrait($montant)
    {
        $ancientSolde = $this->solde;
        parent::retrait($montant);

        if($this->solde != $ancientSolde)
        {
            $this->dernierRetrait = new DateTime();
        }
    }
}

?>