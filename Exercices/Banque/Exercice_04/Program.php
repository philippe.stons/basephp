<?php 

require_once 'Models/Courant.php';
require_once 'Models/Epargne.php';
require_once 'Models/Personne.php';
require_once 'Models/Banque.php';

$p = Personne::getOneByID(1);

$c = new Courant("000001", 0, $p);

$c->depot(500);
$c->retrait(200);

$c->update();

?>