<?php 

require_once 'Compte.php';

class Vector implements ArrayAccess
{
    private $content;

    public function __construct()
    {
        $this->content = [];
    }

    // $banque = new Banque("nom");
    // $banque["0001"] = $value;
    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    // $banque = new Banque("nom");
    // if($banque["0001"])
    public function offsetExists($offset) {
        return isset($this->content[$offset]);
    }

    // $banque = new Banque("nom");
    // unset($banque["0001"])
    public function offsetUnset($offset) {
        unset($this->content[$offset]);
    }

    // $banque = new Banque("nom");
    // $banque["0001"]->depot();
    public function offsetGet($offset) {
        /*
        if(isset($this->content[$offset]))
        {
            return $this->content[$offset];
        }
        return null;

        Ternary operator
        */
        return isset($this->content[$offset]) ? $this->content[$offset] : null;
    }

    public function append($value)
    {
        $this->content[] = $value;
    }
}

?>