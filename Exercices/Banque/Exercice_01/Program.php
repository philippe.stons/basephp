<?php 

require_once 'Models/Compte.php';
require_once 'Models/Personne.php';

$p = new Personne("John", "Doe", new DateTime("2000-01-01"));
$c = new Compte("0000001", 0.0, 0.0, $p);

$c->depot(500);
$c->retrait(300);

echo $c->getSolde();

?>