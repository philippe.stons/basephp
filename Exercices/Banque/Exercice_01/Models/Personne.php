<?php 

// CREATE TABLE personne ( personne_id INT NOT NULL AUTO_INCREMENT, nom VARCHAR(25), prenom VARCHAR(25), dateNaiss DATETIME, PRIMARY KEY(personne_id) )

class Personne
{
    public $nom;
    public $prenom;
    public $dateNaiss;

    public function __construct($nom, $prenom, DateTime $dateNaiss)
    {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->dateNaiss = $dateNaiss;
    }
}

?>