<?php 

function carre($nbr)
{
    return $nbr * $nbr;
}

function rechercheTab($array, $size, $val)
{
    for($i = 0; $i < $size; $i++)
    {
        if($array[$i] == $val)
        {
            return $i;
        }
    }

    return -1;
}

$ar = [5, 7, 6, 9, 8, 2, 11, 3, 12];

echo carre(9) . "\n";

$c = carre(9);

if($c == 81)
{
    echo "coucou\n";
}

echo rechercheTab($ar, count($ar), 7) . "\n";
echo rechercheTab($ar, count($ar), 42) . "\n";
echo rechercheTab($ar, count($ar), 2) . "\n";

?>