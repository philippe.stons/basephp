<html>
    <head>
        <title>Mon super titre</title>
    </head>
    <body>
        <?php 
            $arrayMonth = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", 
            "Aout", "Septembre", "Octobre", "Novembre", "Décembre"];
        ?>
        <form action="Exercice_6_7.php" method="GET">
            <div>
                <input type="radio" id="chronologique" name="chr" value="chronologique" 
                    <?php   if(isset($_GET["chr"]))
                            {
                                if($_GET["chr"] == "chronologique") echo "checked";
                            } 
                    ?>/>
                <label for="chronologique">chronologique</label>
    
                <input type="radio" id="inverse" name="chr" value="inverse"
                    <?php   if(isset($_GET["chr"]))
                            {
                                if($_GET["chr"] == "inverse") echo "checked";
                            } 
                    ?>/>
                <label for="inverse">inverse</label>
            </div>
            <div>
                <label for="month">Months </label>
                <select id="month" name="month">
                    <?php
                        $selected = $arrayMonth[0];
    
                        if(isset($_GET["month"]))
                        {
                            $selected = $_GET["month"];
                        }
                        foreach($arrayMonth as $month)
                        {
                            echo "<option value=\"$month\"";
                            if($selected == $month)
                            {
                                echo " selected";
                            }
                            echo ">$month</option>";
                        }
                    ?>
                </select>
            </div>
            <input type="submit" name="submit" value="submit"/> 
        </form>
        <table>
            <tr>
                <th> Months </th>
            </tr>
            <?php
                if(isset($_GET["chr"]))
                {
                    if($_GET["chr"] == "inverse")
                    {
                        for($i = count($arrayMonth) - 1; $i >= 0; $i--)
                        {
                            echo "<tr><td>" . $arrayMonth[$i] . "</td></tr>";
                        }
                    } else 
                    {
                        foreach($arrayMonth as $month)
                        {
                            echo "<tr><td>$month</td></tr>";
                        }
                    }
                }
            ?>
        </table>
        <?php 
            if(isset($_GET["month"]))
            {
                echo "<br/><br/> selected month = " . $_GET["month"];
            }
        ?>
    </body>
</html>