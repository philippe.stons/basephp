<?php 

class Animal
{
    public $nom;

    public function Manger()
    {
        echo "Je mange! :D \n";
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    function __toString()
    {
        return "Mon nom est $this->nom \n";
    }
}

$monChat = new Animal();
$monChat->nom = "tralal";
$monChat->Manger();

if($monChat instanceof Animal)
{
    echo $monChat;
} else 
{
    echo "Je ne suis pas un animal\n";
}

echo get_class($monChat);

var_dump($monChat);

?>