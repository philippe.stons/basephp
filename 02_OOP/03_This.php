<?php 

class Animal
{
    public $nom;

    public function Manger()
    {
        echo "Je mange! :D \n";
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
        $this->Manger();
    }
}

$monChat = new Animal();
$monChat->nom;
$monChat->Manger();

?>