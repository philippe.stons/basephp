<?php 

class Animal
{
    public $nom;

    public function Manger()
    {
        echo "Je mange! :D \n";
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    function __toString()
    {
        return "Mon nom est $this->nom \n";
    }
}

$monChat = new Animal();
$monChat->nom = "Lucky";
$monChat->Manger();

echo $monChat;

?>