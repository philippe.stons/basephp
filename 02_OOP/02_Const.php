<?php 

class Animal
{
    public $nom;

    const REIGNE_ANIMAL = 1;

    public function Manger()
    {
        echo "Je mange! :D \n";
    }
}

$monChat = new Animal();
$monChat->nom;
$monChat->Manger();

echo Animal::REIGNE_ANIMAL;

?>