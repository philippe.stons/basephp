<?php

use Chat as GlobalChat;

abstract class Command
{
    abstract public function Execute();
}

class FileButton extends Command
{
    public function Execute()
    {

    }
}

abstract class Animal
{
    public $nom;

    public function Manger()
    {

    }

    public function Dormir()
    {
        echo "je dors";
    }
    
    abstract public function Crier();
}

final class Chat extends Animal
{
    final public function Dormir()
    {
        echo "je dors 15h par jours\n";
        parent::Dormir();
    }

    public function Crier()
    {
        echo "Miaou";
    }
}

class Siamois extends Chat
{
    public function Dormir()
    {
        
    }
}

$chat = new Chat();

$chat->Dormir();

?>