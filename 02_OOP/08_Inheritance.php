<?php

class Animal
{
    public $nom;
    public $age;
    protected $lala;
    private static $staticVar;

    /*
    Objet 1 
        - nom = toto
        - age
        ...
    objet 2
        - nom = tata
        - age
        ...
    ...

    
    ---------------------

    staticVar = trlala
    */

    public function __construct($nom)
    {
        $this->nom = $nom;
        self::$staticVar = "valeur de base";
    }

    public static function staticFunction()
    {
        echo "print this\n";
    }

    public function Manger()
    {
        echo "Je mange! :D \n";
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    public function printStaticVar()
    {
        echo self::$staticVar . "\n";
    }

    function __toString()
    {
        return "Mon nom est $this->nom \n";
    }
}

class Chat extends Animal
{
    public function __construct()
    {
        
    }
}

?>