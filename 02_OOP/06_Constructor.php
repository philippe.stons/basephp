<?php 

class Animal
{
    public $nom;

    public function __construct($nom)
    {
        $this->nom = $nom;
    }

    public function Manger()
    {
        echo "Je mange! :D \n";
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    function __toString()
    {
        return "Mon nom est $this->nom \n";
    }
}

class Test
{
}

function printAnimal(Animal $animal)
{
    print $animal;
}

function testPrint(string $test, int $test2)
{

}

$monChat = new Animal("Lucky");
$monPoison = new Animal("Bubulle");
$monChat->Manger();
$test = new Test();

printAnimal($monChat);
printAnimal($monPoison);
printAnimal($test);

?>