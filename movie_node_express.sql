-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 22, 2021 at 06:05 PM
-- Server version: 5.7.31
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `movie_node_express`
--

-- --------------------------------------------------------

--
-- Table structure for table `actor`
--

DROP TABLE IF EXISTS `actor`;
CREATE TABLE IF NOT EXISTS `actor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `actor`
--

INSERT INTO `actor` (`id`, `first_name`, `last_name`) VALUES
(1, 'Anton', 'Bruckner'),
(2, 'Johann Christian', 'Bach'),
(3, 'Jean Sebastien', 'Bach'),
(4, 'Johann', 'Brahms'),
(5, 'Igor', 'Stravinsky'),
(6, 'Dimitri', 'Chostakovitch'),
(8, 'Hector', 'Berlioz'),
(9, 'Wolfgang Amadeus', 'Mozart'),
(10, 'Joseph', 'Haydn'),
(11, 'Edward', 'Elgar'),
(12, 'Dietrich', 'Buxtehude'),
(13, 'Heinrich', 'Biber'),
(14, 'Antonio', 'Vivaldi'),
(15, 'Georg Freidrich', 'Handel'),
(16, 'Richard', 'Wagner'),
(17, 'Claude', 'Debussy'),
(18, 'Antonin', 'Dvorak'),
(19, 'Franz', 'Schubert'),
(20, 'Maurice', 'Ravel'),
(21, 'Giuseppe', 'Verdi'),
(22, 'Piotr Ilitch', 'Tchaikovski'),
(23, 'Franz', 'Liszt'),
(24, 'Frederic', 'Chopin'),
(25, 'Gabriel', 'Faure'),
(26, 'Camille', 'Saint-Saens'),
(27, 'Bela', 'Bartok'),
(28, 'Felix', 'Mendelssohn'),
(29, 'Serguei', 'Prokofiev'),
(30, 'Serguei', 'Rachmaninov'),
(31, 'Robert', 'Schumann'),
(32, 'Clara', 'Schumann'),
(33, 'Gioachino', 'Rossini'),
(34, 'Fanny', 'Mendelssohn'),
(35, 'Cecile', 'Chaminade'),
(36, 'George', 'Bizet'),
(37, 'Niccolo', 'Paganini'),
(42, 'Sergiu', 'Celibdache'),
(43, 'Herbert', 'Von Karajan'),
(48, 'asdasdqdsqds', 'dsadsqsqsdqdwsq'),
(49, 'asdasdsadsad', 'dqwsdqwqsqsdqd'),
(50, 'qwertyuiiopp[', 'qasdfghjkl'),
(51, 'qweqweq', 'qweqweqweqwe'),
(52, 'poiuytrewq', 'qwertyuiop'),
(53, 'testactor', 'testactor');

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

DROP TABLE IF EXISTS `film`;
CREATE TABLE IF NOT EXISTS `film` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `annee_sortie` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`id`, `title`, `description`, `annee_sortie`) VALUES
(1, 'Star wars 1', 'La menace fantôme', '1977-01-01'),
(17, 'Star wars 2', 'L\'attaque des clones', '1980-01-01'),
(18, 'Star wars 3', 'La revanche des Siths', '1983-01-01'),
(19, 'Star wars 4', 'Un nouvel espoir', '1999-01-01'),
(20, 'Star wars 5', 'L\'empire contre attaque', '2002-01-01'),
(21, 'Star wars 6', 'Le retour du Jedi', '2005-01-01'),
(22, 'Star wars 7', 'Le réveil de la force', '2015-01-01'),
(23, 'Star wars 8', 'Les derniers Jedi', '2017-01-01'),
(24, 'Star wars 9', 'L\'ascension de Skywalker', '2019-01-01'),
(25, 'Star wars', 'The clone wars', '2008-01-01'),
(26, 'Star wars', 'Rogue one', '2016-01-01'),
(27, 'Star wars', 'Solo', '2018-01-01'),
(28, 'Star wars', 'Rogue Squardon', '2023-01-01'),
(30, 'A beautiful mind', 'Ron Howard', '2001-12-13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(30) NOT NULL,
  `admin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `admin`) VALUES
(1, 'test', 'test', 'youpiiii@youpi.be', 1),
(2, 'rmdir', 'sudosu', 'rm@linux.org', 0),
(3, 'BWV1001', 'fugaAllegro', 'violin@instrument.be', 0),
(5, 'phil', 'superSecurePassword', 'securityexpert@supersecure.be', 1),
(6, 'admin', 'admin', 'admin@admin.com', 1),
(7, 'philasdasd', 'asdqdqwdqwsqdsq', 'test@test.test', 0),
(8, 'qwerty', 'qwerty', 'qwerty@asdf.be', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
