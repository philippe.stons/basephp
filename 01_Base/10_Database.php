<?php 

// $serverName = "localhost";
// $username = "root";
// $password = "";
// $conn = mysqli_connect($serverName, $username, $password, "movie_node_express");

// $req = "SELECT * FROM film";

// $res = $conn->query($req);

// // obtenir les resultat ligne par ligne
// $data = mysqli_fetch_array($res);
// var_dump($data);

// $data = mysqli_fetch_array($res);
// var_dump($data);

// echo "fetch all \n";

// // obtenir la tous les resultats (ici la suite des deux precedent)
// $results = mysqli_fetch_all($res, MYSQLI_ASSOC);

// var_dump($results);

echo "\n\n PDO \n";

$pdo = new PDO('mysql:host=127.0.0.1;dbname=movie_node_express;charset=utf8', // database info
    "root", // username
    ""); // password

$stmt = $pdo->prepare("SELECT * FROM film");
$stmt->execute();

$data = $stmt->fetchAll();

var_dump($data);

echo "exemple request where \n";

$stmt = $pdo->prepare("SELECT * FROM users WHERE username=:username AND email=:email");
$stmt->execute(["username" => "test", "email" => "youpiiii@youpi.be"]);

$data = $stmt->fetch();

var_dump($data);

echo $data["username"] . "\n";

echo "\n\n\n\n\n";

$stmt = $pdo->prepare("SELECT first_name, last_name FROM actor");
$stmt->execute();

$data = $stmt->fetch();
echo $data["first_name"] . " " . $data["last_name"] . " \n";
$data = $stmt->fetch();
echo $data["first_name"] . " " . $data["last_name"] . " \n";
$data = $stmt->fetch();
echo $data["first_name"] . " " . $data["last_name"] . " \n";
$data = $stmt->fetch();
echo $data["first_name"] . " " . $data["last_name"] . " \n";

var_dump($data);

echo "\n\n\n\n\nFETCHALL\n";

$stmt = $pdo->prepare("SELECT * FROM users");
$stmt->execute();

$datas = $stmt->fetchAll();


var_dump($datas);

foreach($datas as $data)
    echo $data["username"] . " " . $data["password"] . " \n";

$stmt = $pdo->prepare("SELECT * FROM users");
$stmt->execute();

$data = $stmt->fetchAll();


var_dump($data);
echo $data[0]["username"] . " " . $data[0]["password"] . " \n";

?>