<?php 
    $a = 5;
    $b = 4;

    if($a >= $b)
    {
        echo "true";
    }

    if($a == $b) 
        echo "a == b";

    $message = readline("Enter a string: ");
    $test = (int)readline("Enter an int: ");

    echo $message . "\n";
    echo $test . "\n";

    echo "while loop \n";
    while($a < 10)
    {
        $a++;
        echo "$a \n";
    }

    echo "do while loop \n";
    do
    {
        $b++;
        echo "$b \n";
    }while($b < 4);

    echo "for loop \n";
    for($i = 0; $i < 10; $i++)
    {
        echo "$i \n";
    }

    $var = 0;
    switch($var)
    {
        case 0:
            echo "var == 0";
            break;
        case 1:
            echo "var == 1";
            break;
        case 2:
            echo "var == 2";
            break;
        default:
            echo "unkown value";
            break;
    }
?>